package INF102.lab1.triplicate;

import java.util.List;

import java.util.stream.Collectors;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        List<T> sortedList = list.stream().sorted().collect(Collectors.toList());

        for (int i = 0; i < sortedList.size()-2; i++) {
            T checkValue = sortedList.get(i);
            if(!checkValue.equals(sortedList.get(i + 1))) continue;
            if(!checkValue.equals(sortedList.get(i + 2))) continue;
            return checkValue;
        }
        return null;
    }
    
}
